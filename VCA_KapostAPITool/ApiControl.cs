﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Helpers;

namespace VCA_KapostAPITool
{
    public class APIControl
    {
        private string url;
        private Uri urlUri;
        private string username;
        private string password;

        public APIControl(string url, string username, string password = "")
        {
            this.url = url;
            this.urlUri = new Uri(url);
            this.username = username;
            this.password = password;
        }

        public T PutContent<T>(string json, string id, string apiPath = null)
        {
            apiPath = apiPath ?? Path.Combine(this.GetContentPath());
            Uri fullPath = new Uri(urlUri, apiPath + @"/" + id);
            return Json.Decode<T>(this.PutCallAPI(fullPath, json));
        }


        public T PostContent<T>(NameValueCollection paramCollection, string apiPath = null)
        {
            apiPath = apiPath ?? Path.Combine(this.GetContentPath());
            Uri fullPath = new Uri(urlUri, apiPath);
            return Json.Decode<T>(this.PostCallAPI(fullPath, paramCollection));
        }

        public T GetContent<T>(string id, string apiPath = null)
        {
            apiPath = apiPath ?? Path.Combine(this.GetContentPath());
            return this.Get<T>(apiPath + @"/" + id);
        }

        public T GetListContent<T>(string apiPath = null)
        {
            apiPath = apiPath ?? Path.Combine(this.GetContentPath());
            return this.Get<T>(apiPath);
        }

        public T Get<T>(string apiPath)
        {
            Uri fullPath = new Uri(urlUri, apiPath);
            return Json.Decode<T>(this.GetCallAPI(fullPath));
        }

        private string DeleteCallAPI(Uri fullUrl)
        {
            using (WebClient webClient = new WebClient())
            {
                // Specify SSL
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                // Build credentials and add to headers
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(this.username + ":" + this.password));
                webClient.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);
                webClient.Headers[HttpRequestHeader.ContentType] = "application/json";

                // Make call
                string response = webClient.UploadString(fullUrl, "DELETE");
                return response;
            }
        }

        private string PutCallAPI(Uri fullUrl, string json)
        {
            using (WebClient webClient = new WebClient())
            {
                // Specify SSL
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                // Build credentials and add to headers
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(this.username + ":" + this.password));
                webClient.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);
                webClient.Headers[HttpRequestHeader.ContentType] = "application/json";

                // Make call
                string response = webClient.UploadString(fullUrl, "PUT", json);
                return response;
            }
        }

        private string PostCallAPI(Uri fullUrl, NameValueCollection paramCollection)
        {
            using (WebClient webClient = new WebClient())
            {
                // Specify SSL
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                // Build credentials and add to headers
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(this.username + ":" + this.password));
                webClient.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);
                //webClient.Headers[HttpRequestHeader.ContentType] = "application/json";

                // Make call
                byte[] response = webClient.UploadValues(fullUrl, paramCollection);
                string strResponse = Encoding.UTF8.GetString(response);
                return strResponse;
            }
        }

        private string GetCallAPI(Uri fullUrl)
        {
            using (WebClient webClient = new WebClient())
            {
                // Specify SSL
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                // Build credentials and add to headers
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(this.username + ":" + this.password));
                webClient.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);

                // Make call
                string response = webClient.DownloadString(fullUrl);
                return response;
            }
        }

        private string GetContentPath()
        {
            return @"api/v1/content";
        }        
    }
}
