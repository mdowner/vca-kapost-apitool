﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using VCA_KapostAPITool.JSON_Classes;

namespace VCA_KapostAPITool
{
    class Program
    {
        static void Main(string[] args)
        {
            string subdomain = "visitcaliforniacommunications";
            string url = @"https://" + subdomain + ".kapost.com";
            APIControl api = new APIControl(url, "yyXikxvKnCAzpq6NkyPw");

            NameValueCollection jsonPost = new NameValueCollection()
            {
                { "title", "test+title" },
                { "body", "test+body" },
                { "privacy", "members" },
                { "is_draft", "false" },
                { "invitee_ids[]", "1" },
                { "invitee_ids[]", "2" }
            };
            
            POSTRESPONSE.Rootobject jsonResponse = api.PostContent<POSTRESPONSE.Rootobject>(jsonPost);

            GET.Rootobject jsonGet = api.GetContent<GET.Rootobject>(jsonResponse.response.id);

            GETLIST.Rootobject jsonClass = api.GetListContent<GETLIST.Rootobject>();
        }
    }
}
