﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VCA_KapostAPITool.JSON_Classes
{
    public class POSTRESPONSE
    {
        public class Rootobject
        {
            public Response response { get; set; }
        }

        public class Response
        {
            public string id { get; set; }
            public string slug { get; set; }
            public string title { get; set; }
            public string creator_id { get; set; }
            public string assignee_id { get; set; }
            public string[] campaign_ids { get; set; }
            public string preferred_title { get; set; }
            public string next_task { get; set; }
            public string state { get; set; }
            public bool archived { get; set; }
            public DateTime updated_at { get; set; }
            public string factory_url { get; set; }
            public Content_Type content_type { get; set; }
            public string progress_stage { get; set; }
            public string content { get; set; }
            public string[] tags { get; set; }
            public object[] custom_fields { get; set; }
            public string published_url { get; set; }
            public object published_date { get; set; }
            public object library_tracking_url { get; set; }
            public Last_Updated_By last_updated_by { get; set; }
            public object[] attachments { get; set; }
            public object media_url { get; set; }
            public int content_number { get; set; }
            public object[] persona_ids { get; set; }
            public object[] stage_ids { get; set; }
        }

        public class Content_Type
        {
            public string background_color { get; set; }
            public string body_type { get; set; }
            public string display_name { get; set; }
            public string field_name { get; set; }
            public string icon { get; set; }
            public string icon_css_class { get; set; }
            public string id { get; set; }
            public string newsroom_id { get; set; }
            public DateTime updated_at { get; set; }
        }

        public class Last_Updated_By
        {
            public string id { get; set; }
            public string email { get; set; }
            public string name { get; set; }
            public string avatar_feed_url { get; set; }
            public string avatar_thumbnail_url { get; set; }
            public string avatar_profile_large_url { get; set; }
            public string avatar_profile_url { get; set; }
            public string slug { get; set; }
            public DateTime created_at { get; set; }
            public DateTime updated_at { get; set; }
            public object bio { get; set; }
            public string short_bio { get; set; }
            public bool visitor { get; set; }
            public string tz { get; set; }
            public bool follow_discussion_allowed { get; set; }
            public bool superadmin { get; set; }
            public object[] tours_seen { get; set; }
        }
    }
}
