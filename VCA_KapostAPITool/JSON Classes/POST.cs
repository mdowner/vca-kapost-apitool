﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VCA_KapostAPITool.JSON_Classes
{
    public class POST
    {
        public string title { get; set; }
        public string body { get; set; }
        public string assignee_id { get; set; }
        public string privacy { get; set; }
        public int[] invitee_ids { get; set; }
        public bool is_draft { get; set; }
    }
}
