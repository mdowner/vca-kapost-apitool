﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VCA_KapostAPITool.JSON_Classes
{
    public class GET
    {
        public class Rootobject
        {
            public Response response { get; set; }
        }

        public class Response
        {
            public string id { get; set; }
            public string slug { get; set; }
            public string title { get; set; }
            public string creator_id { get; set; }
            public string assignee_id { get; set; }
            public object[] campaign_ids { get; set; }
            public string preferred_title { get; set; }
            public string next_task { get; set; }
            public string state { get; set; }
            public bool archived { get; set; }
            public DateTime updated_at { get; set; }
            public string factory_url { get; set; }
            public Content_Type content_type { get; set; }
            public string progress_stage { get; set; }
            public int content_number { get; set; }
        }

        public class Content_Type
        {
            public string background_color { get; set; }
            public string body_type { get; set; }
            public string display_name { get; set; }
            public string field_name { get; set; }
            public string icon { get; set; }
            public string icon_css_class { get; set; }
            public string id { get; set; }
            public string newsroom_id { get; set; }
            public DateTime updated_at { get; set; }
        }
    }
}
